/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package urat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.swing.JOptionPane;
import urat.SocketConn;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Server {

	public static void main(String[] args) throws Exception {
		List<ServerConnection> servers = new ArrayList<>();

		Thread thread = new Thread(() -> accept(servers));
		thread.start();

		while (true) {
			String command = JOptionPane.showInputDialog("Type command to run");
			if (command == null) {
				continue;
			}
			System.out.println(command);
			servers.stream()
					.map(s -> s.getCommands())
					.forEach(commands -> {
						synchronized (commands) {
							commands.add(command);
							commands.notify();
						}
					});
		}
	}

	public static void accept(List<ServerConnection> servers) {
		try {
			ServerSocketFactory sockFactory = ServerSocketFactory.getDefault();
			ServerSocket sock = sockFactory.createServerSocket(SocketConn.PORT);

			while (true) {
				Socket accept = sock.accept();
				ServerConnection server = new ServerConnection(accept, servers);

				Thread thread = new Thread(server);
				thread.start();
			}
		} catch (IOException ex) {
			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
