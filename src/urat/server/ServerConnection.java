/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package urat.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ServerConnection implements Runnable {

	private final Socket sock;
	private final List<String> commands;
	private final List<ServerConnection> servers;

	public ServerConnection(Socket sock, List<ServerConnection> servers) {
		this.sock = sock;
		this.servers = servers;
		synchronized (servers) {
			servers.add(this);
		}
		commands = new ArrayList<>();

		System.out.println(sock.getRemoteSocketAddress() + " connected");
		printClients();
	}

	@Override
	public void run() {
		try {
			while (true) {
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
				String command = getCommand();
				writer.write(command);
				writer.write('\n');
				writer.flush();
			}
		} catch (IOException | InterruptedException ex) {
//			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			synchronized (servers) {
				servers.remove(this);
				System.out.println(sock.getRemoteSocketAddress() + " disconnected");
				printClients();
			}
		}
	}

	public List<String> getCommands() {
		return commands;
	}

	private String getCommand() throws InterruptedException {
		synchronized (commands) {
			while (commands.isEmpty()) {
				commands.wait();
			}
			return commands.remove(0);
		}
	}

	private void printClients() {
		System.out.println(servers.size() + " Clients connected.");
	}
}
